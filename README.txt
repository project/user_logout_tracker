INTRODUCTION
------------

Custom module that tracks the user logout activity.


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

* The module has menu. There is no configuration. When
  enabled, the module will prevent the links from appearing.
  To get the links back, disable the module and clear caches.

* You can find the menu here

  Configuration >> People >> User Logout Tracker
